USE exemple;

-- Vider les tables
SET FOREIGN_KEY_CHECKS =0;
truncate articles;
truncate clients;
SET FOREIGN_KEY_CHECKS =1;

-- Insertion de données => INSERT
-- Insérer une ligne en spécifiant toutes les colonnes
INSERT INTO articles VALUES (1,'Smartphone',450.0);

-- Insérer une ligne en spécifiant les colonnes souhaitées
INSERT INTO articles (reference,description,prix_unitaire) VALUES (2,'TV 4K',600.0);

-- Insérer plusieurs lignes à la fois
INSERT INTO articles (reference,description,prix_unitaire) VALUES
 (300,'Souris Gamer',30.0),
(1500,'Clavier Azerty',9.0),
(123,'Pc Portable',2350.0),
(56,'Câble HDMI',15.0),
(500,'Smartphone',1500.0);

INSERT INTO clients (prenom,nom,article) VALUES 
('John','Doe',2),
('Jane','Doe',1),
('Alan','Smithee',1500);

-- Supprimer des données => DELETE, TRUNCATE

-- Supression de la ligne qui a pour reference 300 dans la table articles
DELETE FROM articles WHERE reference = 300;

-- Supression de toutes les lignes qui on un prix unitaire supérieur à 1000
DELETE FROM articles WHERE prix_unitaire >1000;

-- Supression de toutes les lignes de la table articles  => ne ré-initialise pas la colonne AUTO_INCREMENT
DELETE FROM articles;

-- SET FOREIGN_KEY_CHECKS =0; -- désactiver la vérification des clés étrangères (MYSQL/ MARAIDB)
-- Supression de toutes les lignes de la table articles  => remet les colonnes AUTO_INCREMENT à 0
-- truncate clients;
-- SET FOREIGN_KEY_CHECKS =1; --réactiver la vérification des clés étrangères

-- Modification des données => UPDATE
-- Modification du prix de l'article qui a pour reference 1500
UPDATE articles SET prix_unitaire=10.0 WHERE reference =1500;

-- Modification de toutes les lignes de table articles qui ont un prix <15.0 => nouvelle valeur 14.0
UPDATE articles SET prix=14.0 WHERE prix<15.0;

-- Modification de tous les prix > 100.0 augmentation de 10%
UPDATE articles SET prix_unitaire=prix_unitaire*1.1 WHERE prix_unitaire >100.0;
