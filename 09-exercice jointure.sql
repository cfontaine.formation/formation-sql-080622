-- Faire une jointure interne entre la ville et le pays et on veut afficher le nom de la ville et du pays
SELECT villes.nom, pays.nom FROM villes 
INNER JOIN pays ON villes.pays = pays.id_pays ; 

-- Faire une jointure interne entre la ville et le continent et on veut afficher le nom de la ville et du continent
SELECT villes.nom, continents.nom FROM villes
INNER JOIN continents ON villes.continent = continents.id_continent ;

-- Afficher tous les pays sans ville
SELECT country.name FROM country
LEFT JOIN city ON country_code = code WHERE city.id  IS NULL ;

-- Afficher les pays et les villes classer par pays puis par ville par ordre alphabétique, ne pas afficher les pays sans ville
SELECT country.name , city.name FROM country
INNER JOIN city  ON country_code = code ORDER BY country.name,city.name;