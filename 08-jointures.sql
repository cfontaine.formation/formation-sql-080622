-- Jointure interne => INNER JOIN
-- Relation 1,n
SELECT prenom,auteurs.nom,pays.nom FROM auteurs INNER JOIN pays ON nation =pays.id; 

SELECT prenom,auteurs.nom,pays.nom FROM auteurs INNER JOIN pays ON nation =pays.id WHERE pays.nom='France';

SELECT titre, nom FROM livres INNER JOIN genres ON genre= genres.id;

-- Relation n,n
SELECT prenom,auteurs.nom,titre,genres.nom  FROM auteurs 
INNER JOIN livre2auteur ON auteurs.id=livre2auteur.id_auteur
INNER JOIN livres ON livre2auteur.id_livre =livres.id
INNER JOIN genres ON genre =genres.id;

-- Produit cartésien => CROSS
SELECT titre,genres.nom FROM livres CROSS JOIN genres; 

-- Jointures externes
SELECT titre,nom FROM livres RIGHT JOIN genres ON genre=genres.id;
SELECT titre,nom FROM livres RIGHT JOIN genres ON genre=genres.id WHERE titre IS NULL;

SELECT titre,nom FROM genres LEFT JOIN livres ON genre=genres.id;
SELECT titre,nom FROM genres LEFT JOIN livres ON genre=genres.id WHERE titre IS NULL;

SELECT titre,nom FROM livres FULL JOIN genres ON genre=genres.id;

-- Jointure naturelle

SELECT titre, genres.nom FROM livres NATURAL JOIN genres;


-- auto jointure
CREATE TABLE salaries(
	id INT PRIMARY KEY AUTO_INCREMENT,
	prenom VARCHAR(50),
	nom VARCHAR(50),
	manager INT,
	CONSTRAINT fk_manager 
	FOREIGN KEY (manager)
	REFERENCES salaries(id)
);

INSERT INTO salaries (prenom,nom,manager) VALUES
('John','DOE',NULL),
('Marcel','Dupont',1),
('Jean','Durant',1),
('JO','Dalton',NULL),
('Alan','smithee',4);

SELECT employee_t.prenom,employee_t.nom, manager_t.prenom, manager_t.nom
FROM salaries AS employee_t
LEFT JOIN salaries AS manager_t ON employee_t.manager = manager_t.id ;