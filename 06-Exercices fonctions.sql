USE meteo;

-- Écrire une requête pour générer un code qui a pour forme les 3 première lettres de la ville concaténé avec la chaine '0000'et le nombre de caractère de la ville et séparé par -
SELECT concat_ws('-',LEFT(nom,3),'0000',LENGTH(nom)) AS code FROM villes;

-- Écrire une requête pour trouver la température du matin maximal pour l'année 2010
SELECT MAX(temp_matin)  FROM meteo_data WHERE YEAR(jour)=2010;

-- Écrire une requête   afficher la moyenne des pression pour les mois de juillet de chaque année pour Lille id_ville=1
SELECT YEAR(jour) AS annee , AVG(pression) AS moyenne_pression FROM meteo_data where ville=1 AND MONTH (jour)='7' GROUP BY YEAR(jour);

USE world;

-- Nombre de pays présents dans la table Country
SELECT COUNT(code) FROM Country; 

-- Afficher les continents et leur  population totale classé du plus peuplé au moins peuplé
SELECT continent , sum(population) AS somme_population FROM country GROUP BY continent ORDER BY somme_population DESC;

-- Nombre de pays par continent
SELECT continent,count(code) AS nb_pays FROM country GROUP BY continent;