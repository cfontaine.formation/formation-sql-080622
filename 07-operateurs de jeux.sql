USE exemple;

CREATE TABLE employee(
id INT PRIMARY KEY AUTO_INCREMENT,
prenom VARCHAR(60),
nom VARCHAR(60),
adresse VARCHAR(255),
salaire_mensuel double
);

CREATE TABLE client(
id INT PRIMARY KEY AUTO_INCREMENT,
prenom VARCHAR(60),
nom VARCHAR(60),
adresse VARCHAR(255),
adresse_livraison VARCHAR(255)
);

INSERT INTO employee (prenom,nom,adresse,salaire_mensuel) value 
('JOHN','DOE','1 rue esquermoise LILLE',2000.0),
('JANE','DOE','25 rue esquermoise LILLE',2500.0),
('JO','DALTON','46 rue des cannonier LILLE',3500.0),
('Alan','SMITHEE','45 rue esquermoise LILLE',1800.0);

INSERT INTO client (prenom,nom,adresse,adresse_livraison) value 
('JOHN','DOE','1 rue esquermoise LILLE','idem'),
('YUL','Guilbert','46 rue des cannonier LILLE','idem');

SELECT prenom,nom,adresse FROM employee
UNION ALL
SELECT prenom,nom,adresse FROM client;
