-- Déclaration d'une procédure Stockée
DELIMITER $	-- Changer le délémiteur ; -> $
CREATE PROCEDURE proc1()
BEGIN
	SELECT prenom , nom ,naissance, nation FROM auteurs WHERE deces IS  NULL ;  
END $
DELIMITER ;	-- Rétablir le délémiteur $ -> ;

-- Déclaration de variable dans une procédure stockée
DELIMITER $
CREATE PROCEDURE proc2()
BEGIN
	DECLARE var_local INTEGER DEFAULT 10; -- Déclaration d'une variable locale
	SELECT var_local;
	--SELECT @var1;
END $
DELIMITER ;

-- Passage de paramètre en entrée
DELIMITER $
CREATE PROCEDURE proc3(IN id_auteur INTEGER)
BEGIN
	SELECT nom FROM auteurs WHERE id=id_auteur;
END $
DELIMITER ;

-- Passage de paramètre en sortie
DELIMITER $
CREATE PROCEDURE proc4(OUT nb_auteur INTEGER)
BEGIN
	SELECT COUNT(id) INTO nb_auteur FROM auteurs;
END $
DELIMITER ;

-- Condition IF
DELIMITER $
CREATE PROCEDURE proc5(IN vi INTEGER,OUT vo INTEGER)
BEGIN
	IF vi>10 THEN
		SELECT COUNT(id) INTO vo FROM auteurs;
	ELSE
		SELECT -42 INTO vo;
	END IF;
END $
DELIMITER ;

-- Condition CASE
DELIMITER $
CREATE PROCEDURE proc6(IN vi INTEGER,OUT vo INTEGER)
BEGIN
	CASE vi
		WHEN 1 THEN SELECT -42 INTO vo;
		WHEN 2 THEN SELECT 42 INTO vo;
		ELSE SELECT -1 INTO vo;
	END CASE;
END $
DELIMITER ;

-- Boucle Tant que
DELIMITER $
CREATE PROCEDURE proc7(OUT vo INTEGER)
BEGIN
	DECLARE vl INTEGER DEFAULT 0;
	WHILE vl<10 DO
		SET vl = vl + 1;
	END WHILE;
	SELECT vl INTO vo;
END $
DELIMITER ;

-- Suppression de toutes les procédures stockées
-- DROP PROCEDURE IF EXISTS proc1;
-- DROP PROCEDURE IF EXISTS proc2;
-- DROP PROCEDURE IF EXISTS proc3;
-- DROP PROCEDURE IF EXISTS proc4;
-- DROP PROCEDURE IF EXISTS proc5;
-- DROP PROCEDURE IF EXISTS proc6;
-- DROP PROCEDURE IF EXISTS proc7;
