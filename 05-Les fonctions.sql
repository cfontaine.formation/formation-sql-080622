-- Fonction arithmétique
SELECT ceil(1.4) AS ceil, floor(1.4) AS floor, round(1.3338888,3) AS round, truncate(1.33888888,3);

SELECT id, titre ,annee FROM livres ORDER BY rand() LIMIT 5;

-- Fonctions chaines de caractères
-- Valeur en ASCII du caractère a et b
SELECT ascii('a'),ascii('b');

-- Nombre de caractère du nom de chaque auteur
SELECT LENGTH (nom) FROM auteurs;

-- Concaténation du prenom et du nom pour chaque
SELECT concat(prenom, ' ', nom) AS identite FROM auteurs; 

-- Concaténation de chaînes avec un séparateur
SELECT concat_ws(' ',prenom, nom,id) AS identite FROM auteurs;

-- Formater un nombre au format: ###.###.###.### 3 chiffres après la virgule
SELECT FORMAT(162396582.39,3);

-- Insertion d'une chaîne à une position et pour un certain nombre de caractères
SELECT INSERT(prenom,2,2,nom) FROM auteurs;

-- Retourne la position de premier 'er' dans la chaine azerty => 3
SELECT POSITION('er' IN 'azerty');

-- Répète 6 fois une chaîne => erererererer
SELECT REPEAT('er',6);

-- remplace er par un __ dans la chaine azertyer
SELECT REPLACE('azertyer','er','--');

-- inversion  de la chaine hello world
SELECT REVERSE('hello world');

-- 3 premier caractère du prenom et les 3 derniers caractère du prénom des auteurs
SELECT LEFT(prenom,3),RIGHT (prenom,3)FROM auteurs;

-- SPACE(7) => Chaine de 7 espaces, extraire une sous chaine de azerty
-- à la position 3 et de 2 caractères => er 
SELECT SPACE(7),SUBSTR('azerty',3,2);

-- STRCMP =>comparaison de deux chaines de caractères
SELECT STRCMP('azetry','wxcvb'); -- -1 =>  a est avant w 
SELECT STRCMP('azetry','axcvb'); -- 1 => z est après x
SELECT STRCMP('azetry','azetry'); -- 0 => les 2 chaines sont égales 

-- Suprimmer les caractère (espace , tabulation , retour à la ligne) en début et en fin de chaine
SELECT TRIM('         hello world      ');
-- Suprimmer les caractère (espace , tabulation , retour à la ligne) en début
SELECT LTRIM('         hello world      '); 
-- Suprimmer les caractère (espace , tabulation , retour à la ligne) en fin de chaine
SELECT RTRIM('         hello world      ');

-- UPPER => AZERTY, LOWER => azerty
SELECT UPPER('azerty'), LOWER('AZERTY');

-- Requête de date
-- now() et current_timestamp => heure et date courante
-- current_date() => date courante
-- current_time() => heure courante 
SELECT now(),current_timestamp(),current_date(), current_time(); 

-- date => Extrait la date d'un Datetime
SELECT date(now());

-- donne le nombre de jour entre le 1er janvier 2023 et la date courante
SELECT datediff('2023-01-01',current_date());

-- Permet d'ajouter une semaine au 10/06/2022 => 17/06/2022
SELECT date_add('2022-06-10', INTERVAL 1 WEEK);

-- Permet de soustraire 3 ans au 10/06/2022 => 10/06/2019
SELECT date_sub('2022-06-10', INTERVAL 3 YEAR);

-- last_day(now()) => le dernier jour du mois courant
-- last_day('2022-02-01') => 28
-- day extrait le jour du datetime courant  
-- dayname extrait le nom du jour du datetime courant
SELECT last_day(now()),last_day('2022-02-01'), day(now()), dayname(now());

-- permet de spécifier le format de la date 
SELECT date_format(now(),'%D/%M/%Y');

-- modifie la langue pour la session => dayname('2022-06-10') => vendredi 
SELECT @@lc_time_names;
SET lc_time_names = 'fr_FR';

-- YEAR(now()) => extrait l'année d'une date datetime

SELECT YEAR(now()), MONTH('2022-04-04'), dayofmonth(now()),dayofyear(now()); 

SELECT weekday(now()), dayofweek(now()), yearweek(current_date()) ,WEEK(current_date());

SELECT SECOND(current_time()),MINUTE (current_time()), HOUR(current_time());

SELECT timediff('12:30:00',current_time()) ;

SELECT time_format('12:35:51', '%H %i'),time_format(current_time() , '%i|%H'); 

SELECT dayofyear(now())-week(now())*2 AS jour_ouvree;

SELECT TIME_TO_SEC(current_time()), sec_to_time(3600) ;


-- Fonctions d'agrégation
-- Compter le nombre de livres => 142
SELECT count(id) AS nb_livres FROM livres;  

-- Compter le nombre de livres sortie en 1964 => 1
SELECT count(id) AS nb_livres FROM livres WHERE annee=1964; 

-- MAX(annee) => année du livre le plus récent de la bibliothèque
-- MIN(annee) => année du livre le plus ancien de la bibliothèque
-- AVG(annee) => moyenne des année des livres de la bibliothèque
-- MIN(titre), MAX(titre) => l'ordre est les nombres et l'ordre alphabétique 
SELECT MAX(annee), MIN(annee) , AVG(annee), MIN(titre), MAX(titre) FROM livres;

-- MAX(naissance) -> date de naissance de l'auteur le plus jeune
-- MIN(naissance) -> date de naissance de l'auteur le plus vieux
SELECT MAX(naissance), MIN(naissance) FROM auteurs;

-- moyenne des année des livres de la bibliothèque sans nombre après la virgule
SELECT TRUNCATE(AVG(annee),0) FROM livres;

-- COUNT(DISTINCT annee) => Compter le nombre d’enregistrement sans doublon
SELECT COUNT(DISTINCT annee), COUNT(annee)  FROM livres;

-- autres fonction
SELECT id,BIN(id),OCT(id),HEX(id) FROM livres;

SELECT COALESCE (NULL,NULL,4,NULL,5);

SELECT NULLIF(5,67); -- différent -> 5

SELECT NULLIF('HELLO','HELLO'); -- égal -> NULL

SELECT CURRENT_USER(),DATABASE() ,VERSION(); -- mysql

SELECT annee,
CASE 
	WHEN annee>2000 THEN 'Moderne'
	WHEN annee<2000 AND annee>1900 THEN '20 siècle'
	ELSE 'Livre ancien'
END
FROM livres; 

-- regroupement => GROUP BY
SELECT count(id),genre FROM livres GROUP BY genre;

SELECT count(id) ,nation FROM auteurs GROUP BY nation;

SELECT count(id) ,nation FROM auteurs WHERE YEAR(naissance)>1950 GROUP BY nation;

SELECT count(id) AS compte ,nation FROM auteurs GROUP BY nation HAVING compte>5;

SELECT count(id) AS compte ,nation FROM auteurs  WHERE YEAR(naissance)<1950 GROUP BY nation HAVING compte>5;
