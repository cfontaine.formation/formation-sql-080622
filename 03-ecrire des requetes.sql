-- Sélectionner la base des données bibliotheque
USE bibliotheque;

SELECT prenom,nom,naissance FROM auteurs;

-- Quand il y a une ambiguité sur le nom d'une colonne, on ajoute le nom de la table  table.colonne
-- pour différentier les 2 colonnes
SELECT prenom,auteurs.nom,naissance,genres.nom  FROM auteurs,genres;

-- * -> Obtenir toutes les colonnes d’un tableau
SELECT * FROM auteurs;

-- On peut mettre dans les colonnes d'un SELECT une constante ou une colonne qui provient d'un calcul
SELECT titre,'age=', 2022-annee FROM livres; 

-- AS permet de spécifier un Alias pour le nom d'un colonne ou d"une table
-- auteur, genre -> Alias de colonne
SELECT titre, 2022-annee AS age FROM livres;
SELECT titre, 2022-annee age FROM livres;

-- Alias de table
SELECT nom FROM genres AS categorie;
SELECT nom FROM genres categorie;

-- DISTINCT -> permet d’éviter les doublons dans les résultats d’un SELECT
SELECT DISTINCT prenom FROM auteurs;

-- La clause WHERE permet de sélectionner des lignes qui respectent une condition
-- Selection de tous le titres de livre qui sont sortie après 1980
SELECT titre,annee FROM livres WHERE annee>1980;

-- Les opérateurs logiques AND et OR permettent de combiner des conditions
-- Selection des titres, de l'année de sortie du livre qui sont sorties entre 1980 et 1983
SELECT titre,annee FROM livres WHERE annee>1980 AND annee<1983;

-- Sélection du noms et du prénoms  des auteurs américains qui sont vivants
SELECT prenom,nom  FROM auteurs WHERE nation=1 AND deces IS NULL;

-- Selection des titres, de l'année de sortie du livre qui sont sorties en 1964 et en 1992
SELECT titre, annee FROM livres WHERE annee= 1964 OR annee=1992;

-- L'opérateur NOT inverse le resultat
-- Selection des titres, de l'année de sortie du livre qui sont sortie sauf en 1992 (Condition inversée)
SELECT titre, annee FROM livres WHERE NOT annee=1992;

-- L'opérateur XOR ou exclusif 
-- Sélection du noms et du prénoms  des auteurs américains ou des auteurs vivants mais pas des auteurs qui respect les 2 conditions
SELECT prenom,nom  FROM auteurs WHERE nation=1 XOR deces IS NULL;

-- Sélection des auteurs qui ont pour prénom pierre ou john et qui sont nait après le 1er janvier 1920
SELECT prenom, nom, naissance FROM auteurs WHERE (prenom='Pierre' OR prenom='John') AND naissance  >'1920-01-01';

-- IS NULL permet de tester si une valeur est égal à NULL
-- IS NOT NULL  permet de tester si une valeur est différente de NULL
-- Sélection de tous les colonnes de la table auteurs pour les auteurs qui sont vivants => deces = NULL 
SELECT prenom, nom FROM auteurs WHERE deces IS NULL;

-- Sélection de tous les colonnes de la table auteurs pour les auteurs qui sont décédés 
SELECT prenom, nom FROM auteurs WHERE deces IS NOT NULL;

-- Sélection du titre et de l'année pour les livres sortie en 1950,1964,1982 ou 1992
SELECT titre FROM livres WHERE annee IN (1950,1964,1982,1992);

-- Sélection des titres des livres qui sortie entre 1950 et 1975 
SELECT titre FROM livres WHERE annee BETWEEN 1950 AND 1975;

-- Sélection du prénom et du nom des auteurs qui sont nés entre le 1er janvier 1930 et  le 1er janvier 1950
SELECT prenom, nom,naissance  FROM auteurs WHERE naissance  BETWEEN '1930-01-01' AND '1950-01-01';

-- Avec LIKE % représente 0,1 ou plusieurs caratères inconnues
--           _ représente un caratère inconnue
-- Sélection du titre des livres qui commence par D et qui fait 4 caractères (ne tient pas compte de la casse)
SELECT titre FROM livres WHERE titre LIKE 'D___';

-- Sélection du prénoms pour les auteurs qui ont un prénom composé
SELECT prenom FROM auteurs WHERE prenom LIKE '%_-_%';

-- Selection de tous les livres triés par rapport à leur titre par ordre alphabétique
SELECT * FROM livres ORDER BY titre;

-- Selection de tous les auteurs décédés trié par leeur date de naissance décroissant, leur nom (croissant par défaut) et leur prénom décroissant
SELECT prenom, nom FROM auteurs WHERE deces IS NOT NULL ORDER BY naissance DESC,nom ,prenom DESC

-- Selection de tous les auteurs trié par leur prénom (z->a) et leur nom (a->z) 
SELECT prenom, nom FROM auteurs ORDER BY prenom DESC,nom

-- sélectionner les 5 auteurs les plus vieux
SELECT prenom,nom FROM auteurs ORDER BY naissance LIMIT 5;

-- sélectionner les 5 auteurs les plus vieux à partir du 2ème
SELECT prenom,nom FROM auteurs ORDER BY naissance LIMIT 5 OFFSET 2;
-- idem autre syntaxe (MySQL)
SELECT prenom,nom FROM auteurs ORDER BY naissance LIMIT 2,5;
