USE exemple;
-- CREATE TEMPORARY TABLE => crée une table temporaire, qui sera supprimée lorsque l'on quitte la session
CREATE TEMPORARY TABLE personnes
(
	id INTEGER PRIMARY KEY AUTO_INCREMENT,
	nom VARCHAR (255)
);

-- Elle n'apparait pas dans la liste des tables
SHOW tables;

-- mais elle existe le temps de la session 
INSERT INTO personnes(nom)VALUES ('John Doe');

SELECT * FROM personnes;

USE bibliotheque;
-- Table d’Expression Commune (CTE)
-- requête utilisée en tant que table à usage unique
WITH auteur_vivant AS (
	SELECT prenom , nom ,naissance, nation
	FROM auteurs
	WHERE deces IS NOT NULL
),
-- On peut déclarer plusieurs CTE à la suite avant de les utiliser
 auteur_vivant_usa AS (
	SELECT prenom , nom ,naissance
	FROM auteur_vivant -- on peux utiliser une CTE qui vient d'être déclarée avant
	WHERE nation=1
);

-- La CTE ne pourra être utilisée qu’immédiatement après sa déclaration
SELECT * FROM auteur_vivant_usa  WHERE naissance > '1930-01-01';

-- sinon => erreur
-- SELECT * FROM auteur_vivant WHERE naissance > '1930-01-01';

-- Créer une table depuis une sélection de données
-- - générer la structure de la table 
-- - ajouter les données à la table
CREATE TABLE auteurs_vivant 
SELECT
	prenom,
	nom,
	naissance,
	nation
FROM
	auteurs
WHERE
	deces IS NOT NULL;
