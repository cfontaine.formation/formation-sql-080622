-- Écrire la requête pour trouver les villes qui sont en Europe
SELECT id_continent , nom FROM continents;
SELECT nom FROM villes WHERE continent=1; 

-- Écrire la requête pour trouver les villes dont le nom commence par C
SELECT nom FROM villes WHERE nom LIKE 'C%';

-- Écrire la requête pour avoir les infos météorologique  pour Berlin id_ville=10
SELECT * FROM meteo_data WHERE ville=10;

-- Écrire la requête pour avoir les infos météorologique   Le 1 janvier 2011 pour Berlin  id_ville=10
SELECT * FROM meteo_data WHERE ville=10 AND jour='2011-01-01';

-- Écrire la requête pour avoir les infos météorologique entre Le 1 janvier 2011 et 3 mars 2011  pour  Lille id_ville=1
SELECT * FROM meteo_data WHERE ville=1 AND jour BETWEEN '2011-01-01' AND '2011-03-03';

-- Écrire la requête pour trouver toutes les villes qui ne sont pas en Europe
SELECT nom FROM villes WHERE NOT continent=1; -- continent<>1

-- Écrire la requête pour avoir  pour Paris  id_ville=2, l'info météorologique pour Le 1 janvier 2011 , le 3 mars 2009 et le 25 décembre 2010
SELECT * FROM meteo_data WHERE ville=2 AND jour IN ('2011-01-01','2009-03-03','2010-12-25');

-- Écrire la requête pour trouver les villes qui sont en Europe ou bien qui sont en Afrique
SELECT nom FROM villes WHERE continent=1 OR continent=4;

-- Écrire la requête pour calculer la température moyenne pour Lille pour tous les jours de mars 2013 on nommera la colonne temp_moy ,on affichera le jour , la pression et le temp_moy trier les résultat pression + grand à + petite
SELECT (temp_max+temp_min)/2 AS temp_moy,jour, pression FROM meteo_data WHERE ville=1 AND jour BETWEEN '2013-03-01' AND '2013-03-31' ORDER BY pression DESC

USE world;
-- Afficher les 10 premiers pays classé par ordre alphabétiques
SELECT name FROM country ORDER BY name LIMIT 10;

-- Afficher les 10 pays les plus peuplés (nom et population)
SELECT name,population FROM country ORDER BY population DESC LIMIT 10;