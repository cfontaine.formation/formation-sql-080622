-- Créer une vue 
CREATE VIEW v_auteur_vivant AS SELECT prenom , nom ,naissance, nation
FROM auteurs
WHERE deces IS  NULL ;

-- La vue est considéré comme une table
SHOW TABLES; 

-- Utilisation de la vue
SELECT * FROM v_auteur_vivant; 

-- Supression de la vue
DROP VIEW v_auteur_vivant;

-- appel de la procedure stockée proc1
CALL proc1();

-- Afficher la liste des procédures stockées
SHOW PROCEDURE STATUS;

-- Supprimer la procédure stockée proc1
DROP PROCEDURE proc1;

SET @var1='test';
SELECT @var1;

CALL proc2();
-- SELECT var_local;

CALL proc3( 12);

CALL proc4(@var1);
SELECT @var1;

CALL proc5(15,@var1);
SELECT @var1;

CALL proc6(1,@var1);
SELECT @var1;

CALL proc7(@var1);
SELECT @var1;