/* 01 - Conception de base */

-- Suprimer la table exemple si elle existe
DROP DATABASE IF EXISTS exemple;

-- Créer une base de données exemple
CREATE DATABASE exemple;

-- Choisir exemple comme base de données courante 
USE exemple;

-- Créer une table articles qui contient 3 colonnes reference, description et prix
CREATE TABLE article(
	reference INT COMMENT 'reférence de l''article' PRIMARY KEY, --  COMMENT -> Ajout d'un commentaire sur la colonne
																 -- PRIMARY KEY -> reference,clé primaire de la table article 
	description VARCHAR(255) DEFAULT 'Inconnue', -- DEFAULT -> permet de définir une valeur par défaut autre que NULL
	prix DOUBLE NOT NULL -- NOT NULL -> prix ne peut plus être égal à NULL, lorsque l'on ajoutera des données, on sera obligé de données un valeur à prix 
)ENGINE=InnoDB;

-- ENGINE -> Choix du moteur de base de données
-- par défaut InnoDB ->  supporte les transactions et les clés étrangère
-- autre moteur:
--  MEMORY -> stockage en mémoire
-- MyISAM  -> ancien moteur, par défaut pas de support des clés étrangères
--  CSV -> CSV storage engine
-- ...

-- Afficher la liste des tables de la base exemple
SHOW TABLES;

-- Afficher la description de la table vols
DESCRIBE article;

-- Afficher la description de la table articles avec les comentaires et + de détail
SHOW FULL COLUMNS FROM article;

-- Exercice: Gestion de vols
CREATE TABLE pilotes(
	numero_pilote INT,
	prenom VARCHAR(60),
	nom VARCHAR(60),
	nombre_heure_vol INT
	);

CREATE TABLE vols(
	numero_vol INT PRIMARY KEY,
	heure_depart DATETIME,
	ville_depart VARCHAR(100),
	heure_arrivee DATETIME,
	ville_arrivee VARCHAR(100)
);

CREATE TABLE avions(
	numero_avion INT PRIMARY KEY,
	modele VARCHAR(50),
	capacite SMALLINT
);

-- MODIFICATION DES TABLES
-- Renommer une table
RENAME TABLE article TO articles;

-- Ajouter une colonne à une table
ALTER TABLE articles ADD nom VARCHAR(100);

-- Supprimer une colonne à une table
ALTER TABLE articles DROP nom;

-- Changer le type d'une colonne
ALTER TABLE articles MODIFY prix DECIMAL(6,2);  -- 6 chiffres au total et 2 chiffres après la virgule max=9999.99 min=-9999.99

-- Renommer une colonne
ALTER TABLE articles CHANGE prix prix_unitaire DECIMAL(6,2) NOT NULL;

-- Ajout d'une clé primaire (la colonne numero_pilote) à la table pilotes
ALTER TABLE pilotes ADD CONSTRAINT  PRIMARY KEY(numero_pilote);

-- Clé étrangère

-- Relation 1-n
-- ajouter une clé etrangère à une table existante
CREATE TABLE clients(
	id INT PRIMARY KEY AUTO_INCREMENT,
	prenom varchar(50),
	nom varchar(50),
	article INT, -- => clé étrangère 
	CONSTRAINT fk_articles_reference 
	FOREIGN KEY (article)
	REFERENCES articles(reference) ON DELETE CASCADE);
	-- ON DELETE CASCADE propage la supression d'un article et supprime les clients qui le possède

-- Relation n-n
-- Table de jointure entre les clients et les vols
CREATE TABLE clients_vols(
	id_client INT,
	num_vol INT,

	-- contrainte clé étrangère => clients
	CONSTRAINT fk_client_vols1
	FOREIGN KEY (id_client)
	REFERENCES clients(id),
	
	--  contrainte clé étrangère => vols
	CONSTRAINT fk_client_vol2
	FOREIGN KEY (num_vol)
	REFERENCES vols(numero_vol),
	
	-- clé primaire composé des 2 colonnes id_client, num_vol
	CONSTRAINT pk_client_vol
	PRIMARY KEY (id_client,num_vol)
);

-- Ajouter une colonne pour la clé etrangère 
ALTER TABLE vols ADD pilotes INT;

-- ajouter la contrainte de clé étrangère
ALTER TABLE vols ADD 
CONSTRAINT fk_pilotes
FOREIGN KEY (pilotes)
REFERENCES pilotes(numero_pilote);

ALTER TABLE vols ADD avions INT;

ALTER TABLE vols ADD
CONSTRAINT fk_avions
FOREIGN KEY (avions)
REFERENCES  avions(numero_avion);
