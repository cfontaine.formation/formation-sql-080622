-- Requête imbriquée qui retourne un seul résultat => WHERE ou HAVING
SELECT DISTINCT auteurs.prenom, auteurs.nom FROM auteurs 
INNER JOIN livre2auteur ON auteurs.id= livre2auteur.id_auteur 
INNER JOIN  livres ON livres.id= livre2auteur.id_livre 
WHERE livres.id_genre = (
SELECT genres.id_genre  FROM genres
INNER JOIN livres ON genres.id_genre = livres.id_genre 
GROUP BY genres.id_genre ORDER BY COUNT(livres.id) DESC LIMIT 1  );

-- Requête imbriquée qui retourne une colonne entière => IN
SELECT DISTINCT auteurs.prenom, auteurs.nom,genres.nom FROM auteurs 
INNER JOIN livre2auteur ON auteurs.id= livre2auteur.id_auteur 
INNER JOIN  livres ON livres.id= livre2auteur.id_livre
INNER JOIN genres ON livres.id_genre=genres.id_genre
WHERE livres.id_genre IN (
SELECT genres.id_genre  FROM genres
INNER JOIN livres ON genres.id_genre = livres.id_genre 
GROUP BY genres.id_genre HAVING COUNT(livres.id) > 15 );




